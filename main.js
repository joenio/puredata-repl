#!/usr/bin/env node
// puredata-repl/main.js
const repl = require('./repl');
const pd = require('node-libpd');

let ws = null;
try {
  console.log(`Initializing Puredata...`);
  const initialized = pd.init({
    numInputChannels: 0,
    numOutputChannels: 2,
    sampleRate: 48000,
    ticks: 1,
  });
  repl.run(pd);
}
catch(e) {
  console.error('Error initializing Puredata!');
  console.error(e);
  process.exit(1);
}
