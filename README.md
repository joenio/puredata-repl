# puredata-repl

command line interface REPL to load and send messages to puredata patch

it is based on the project: https://github.com/ircam-ismm/node-libpd

**alert** puredata-externals is not supported

## quickstart

```sh
npm install
npm start
```

then load the Pd path and send messages

```sh
puredata> open test/echo-msg.pd
puredata> send test/echo-msg.pd bang Hello World
puredata> bang: symbol Hello\ World
puredata> send test/echo-msg.pd float 1
puredata> float: symbol 1
puredata> close test/echo-msg.pd
```

Pd path `test/echo-msg.pd`:

![puredata echo-msg.pd patch](./echo-msg-pd.png)

add puredata-repl script to `$PATH`

```sh
npm link
```

then the command `puredata-repl` can be executed from the command line from any
directory

# author

Joenio Marques da Costa <joenio@joenio.me>

# license

GPLv3
