// puredata-repl/repl.js
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'puredata> ',
});

let patchs = {};

module.exports = {
  run: (pd) => {
    rl.prompt();
    rl.on('line', (input) => {
        const cmd = input.trim().split(' ')[0];
        const filepath = input.trim().split(' ')[1];
        const args = input.trim().split(' ').slice(2);
        let message = {};
        switch (cmd) {
          case 'open':
            patchs[filepath] = pd.openPatch(filepath);
            break;
          case 'close':
            pd.closePatch(patchs[filepath]);
            break;
          case 'send':
            pd.send(`${patchs[filepath].$0}-${args[0]}`, args.slice(1).join(' '));
            break;
          case 'help':
            console.log('  quit, close, exit');
            break
          case 'quit':
          case 'close':
          case 'exit':
            rl.close()
            break
          default:
            console.log(`puredata? ${input.trim()}`)
        }
        rl.prompt()
      })
      .on('close', () => {
        console.log('\npuredata* bye-bye')
        pd.destroy();
        process.exit(0)
      });
  }
};
